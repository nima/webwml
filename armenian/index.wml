#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

#use wml::debian::translation-check translation="b5580a0473c76bf7aa576b6d929f4422698275ca" maintainer="Armen Dilanyan"

<div id="splash">
  <h1>Debian</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>Համայնքը</h1>
      <h2>Debian-ը մարդկանց համայնքն է:</h2>
      
#include "$(ENGLISHDIR)/index.inc"

      <div class="row">
      <div class="community column">
        <a href="intro/about" aria-hidden="true">
          <img src="Pics/users.svg">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/about">Մարդիկ</a></h2>
        <p>Ով ենք մենք, ինչ ենք մենք անում</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/" aria-hidden="true">
          <img src="Pics/heartbeat.svg">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/">Մեր փիլիսոփայությունը</a></h2>
        <p>Ինչու ենք դա անում և ինչպես ենք դա անում</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/" aria-hidden="true">
          <img src="Pics/user-plus.svg">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">Մասնակցեք, ներդրեք</a></h2>
        <p>Դուք կարող եք լինել այդ ամենի մի մասը:</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="more#community" aria-hidden="true">
          <img src="Pics/list.svg">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="more#community">Ավելին...</a></h2>
        <p>Լրացուցիչ տեղեկություններ Debian համայնքի մասին</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>Օպերացիոն համակարգ</h1>
      <h2>Debian-ը ամբողջական անվճար գործող համակարգ է:</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
        <a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso" class="os-dl-btn"><download></a>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">Ինչու Debian</a></h2>
        <p>Ինչն է DEBIAN-ին դարձնում առանձնահատուկ, որ ձեզ առժե օգտագործել այն</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">Օգտագործողի աջակցություն</a></h2>
        <p>Ստանալ օգնություն և փաստաթղթեր</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">Անվտանգության թարմացումներ</a></h2>
        <p>Debian անվտանգության հայտարարություններ (DSA) <a class="rss_logo" href="security/dsa">RSS</a></p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="more#software" aria-hidden="true">
          <img src="Pics/list.svg">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="more#software">Ավելին...</a></h2>
        <p>Ներբեռնման և ծրագրաշարի լրացուցիչ հղումներ</p>
      </div>
    </div>
  </div>
</div>

<hr>
<!-- The second row of columns on the site. -->
<!-- The News will be selected by the press team. -->

<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>Debian նորություններ և հայտարարություններ</h2>
    </div>

    <p><:= get_top_news() :></p>

    <!-- No more News entries behind this line! -->
    <div class="project-news">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">Բոլոր նորությունները</a> &emsp;&emsp;
        <a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian նորություններ" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Debian նախագծի նորությունները" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Debian անվտանգության խորհրդատվություններ (միայն վերնագրերը)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian անվտանգության խորհրդատվություններ (ամփոփագրեր)" href="security/dsa-long">
:#rss#}

