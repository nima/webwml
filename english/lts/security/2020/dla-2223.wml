<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in package salt, a
configuration management and infrastructure automation software.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11651">CVE-2020-11651</a>

    <p>The salt-master process ClearFuncs class does not properly validate
    method calls. This allows a remote user to access some methods
    without authentication. These methods can be used to retrieve user
    tokens from the salt master and/or run arbitrary commands on salt
    minions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11652">CVE-2020-11652</a>

    <p>The salt-master process ClearFuncs class allows access to some
    methods that improperly sanitize paths. These methods allow
    arbitrary directory access to authenticated users.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2014.1.13+ds-3+deb8u1.</p>

<p>We recommend that you upgrade your salt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2223.data"
# $Id: $
