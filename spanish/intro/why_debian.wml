#use wml::debian::template title="Razones para escoger Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672" maintainer="Laura Arjona Reina"

<p>Hay muchas razones por las que los usuarios eligen Debian como su sistema operativo.</p>

<h1>Razones principales</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian es software libre.</strong></dt>
  <dd>
    Debian está hecho de software libre y de código abierto y siempre será 100%
    <a href="free">libre</a>. Todo el mundo es libre de usarlo, modificarlo y
    distribuirlo. Esta es nuestra principal promesa a <a href="../users">nuestros usuarios</a>. Además, es gratuito.
  </dd>
</dl>

<dl>
  <dt><strong>Debian es un sistema operativo estable y seguro basado en Linux.</strong></dt>
  <dd>
    Debian es un sistema operativo adecuado para un amplio rango de dispositivos incluyendo
    portátiles, ordenadores de escritorio y servidores. A los usuarios les gusta su estabilidad y confiabilidad
    desde 1993. Proporcionamos una configuración predeterminada razonable para cada paquete.
    Los desarrolladores de Debian proporcionan actualizaciones de seguridad para todos los paquetes durante
    su ciclo de vida siempre que es posible.
  </dd>
</dl>

<dl>
  <dt><strong>Debian tiene un soporte de hardware extenso.</strong></dt>
  <dd>
    La mayoría del hardware ya está soportado por el núcleo Linux. Hay disponibles controladores no libres
    para el hardware cuando el software libre no es suficiente.
  </dd>
</dl>

<dl>
  <dt><strong>Debian proporciona actualizaciones de versión sin complicaciones.</strong></dt>
  <dd>
    Debian es bien conocido por sus actualizaciones fáciles y sin complicaciones dentro de su ciclo de publicación
    y también en las actualizaciones a una versión superior.
  </dd>
</dl>

<dl>
  <dt><strong>Debian es la semilla y base de muchas otras distribuciones.</strong></dt>
  <dd>
    Muchas de las distribuciones Linux más populares como Ubuntu, Knoppix, PureOS, 
    SteamOS o Tails eligen Debian como base para su software.
    Debian proporciona todas las herramientas por lo que todo el mundo puede extender los paquetes
    de software del archivo de Debian con sus propios paquetes para sus necesidades.
  </dd>
</dl>

<dl>
  <dt><strong>El proyecto Debian es una comunidad.</strong></dt>
  <dd>
    Debian no es solamente un sistema operativo Linux. El software se desarrolla mediante la colaboración
    de cientos de voluntarios de todo el mundo. Usted puede ser parte de la
    comunidad Debian incluso si no programa ni administra sistemas. Debian se conduce por
    el consenso y la comunidad y tiene una
    <a href="../devel/constitution">estructura de gobierno democrático</a>.
    Al tener todos los desarrolladores de Debian iguales derechos, no puede ser controlada por 
    una sola empresa. Tenemos desarrolladores en más de 60 países y proporcionamos traducciones
    del instalador de Debian para más de 80 idiomas.
  </dd>
</dl>

<dl>
  <dt><strong>Debian tiene múltiples opciones de instalación.</strong></dt>
  <dd>
    Los usuarios finales usarán nuestro
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">CD «en vivo»</a>
    incluyendo el instalador Calamares fácil de usar, que necesita muy poca información o 
    conocimientos previos. Los usuarios más experimentados pueden usar nuestro instalador único con múltiples opciones
    mientras que los expertos pueden personalizar al detalle la instalación o incluso usar una 
    herramienta automatizada de instalación en red.
  </dd>
</dl>

<br>

<h1>Entorno corporativo</h1>

<p>
  Si necesita Debian en un entorno profesional, puede disfrutar de estos
  beneficios adicionales:
</p>

<dl>
  <dt><strong>Debian es confiable.</strong></dt>
  <dd>
    Debian prueba su fiabilidad cada día en miles de escenarios del mundo real
    que van desde portátiles de un único usuario a super colisionadores, bolsa de valores
    y la industria automovilística. También es popular en el mundo académico,
    científico y en el sector público.
  </dd>
</dl>

<dl>
  <dt><strong>Debian tiene muchos expertos.</strong></dt>
  <dd>
    Nuestros mantenedores de paquetes no solo se encargan del empaquetado de Debian y
    de incorporar nuevas versiones del software. A menudo son personas expertas en el software original
    y contribuyen al desarrollo en origen directamente. A veces también participan
    de los proyectos originales.
  </dd>
</dl>

<dl>
  <dt><strong>Debian es seguro.</strong></dt>
  <dd>
    Debian tiene soporte de seguridad para sus versiones estables. Muchas otras
    distribuciones e investigadores de seguridad se apoyan en el gestor de seguridad de Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Soporte a largo plazo.</strong></dt>
  <dd>
    Existe <a href="https://wiki.debian.org/LTS">soporte a largo plazo</a>
    («Long Term Support», LTS) sin coste. Esto posibilita soporte extendido para la versión estable
    durante 5 años y más. Además está también el
    <a href="https://wiki.debian.org/LTS/Extended">Soporte «LTS» extendido</a>, una iniciativa
    que extiende el soporte para un conjunto limitado de paquetes, durante más de 5 años.
  </dd>
</dl>

<dl>
  <dt><strong>Imágenes para la nube.</strong></dt>
  <dd>
    Hay disponibles imágenes oficiales para las principales plataformas de nube. También
    proporcionamos las herramientas y configuración para que pueda construir su propia imagen personalizada 
    para la nube. También puede usar Debian en máquinas virtuales en el escritorio o en un contenedor.
  </dd>
</dl>

<br>

<h1>Desarrolladores</h1>
<p>Debian es ampliamente usado por desarrolladores de hardware y software de todo tipo.</p>

<dl>
  <dt><strong>Sistema de seguimiento de fallos disponible públicamente.</strong></dt>
  <dd>
    Nuestro <a href="../Bugs">sistema de seguimiento de fallos</a> («Bug Tracking System», BTS) de Debian está
    disponible públicamente para cualquiera a través de un navegador web. No escondemos los fallos
    de nuestro software y puede enviar nuevos informes de fallos de manera sencilla.
  </dd>
</dl>

<dl>
  <dt><strong>Internet de las cosas y dispositivos empotrados.</strong></dt>
  <dd>
    Soportamos un amplio rango de dispositivos como Raspberry Pi, variantes de QNAP,
    dispositivos móviles, enrutadores domésticos y muchos ordenadores de placa reducida («Single Board Computer», SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Múltiples arquitecturas de hardware.</strong></dt>
  <dd>
    Existe soporte para una <a href="../ports">larga lista</a> de arquitecturas de CPU
    incluyendo amd64, i386, múltiples versiones de ARM y MIPS, POWER7, POWER8,
    IBM System z, RISC-V. Debian también está disponible para arquitecturas más antiguas
    y arquitecturas de nicho específico.
  </dd>
</dl>

<dl>
  <dt><strong>Gran número de paquetes de software disponible.</strong></dt>
  <dd>
    Debian tiene disponible el mayor número de paquetes instalados
    (actualmente <packages_in_stable>). Nuestros paquetes usan el formato deb que es bien
    conocido por su alta calidad.
  </dd>
</dl>

<dl>
  <dt><strong>Diferentes opciones de versiones.</strong></dt>
  <dd>
    Además de nuestra versión «estable», puede obtener las últimas versiones del software 
    usando las versiones «en pruebas» o «inestable».
  </dd>
</dl>

<dl>
  <dt><strong>Alta calidad con la ayuda de las herramientas de desarrollo y la normativa.</strong></dt>
  <dd>
    Varias herramientas de desarrollo ayudan a mantener la calidad en un alto estándar y
    nuestra <a href="../doc/debian-policy/">normativa</a> define los requisitos
    técnicos que cada paquete debe satisfacer para ser incluido en la
    distribución. Nuestro sistema de integración continua ejecuta el software autopkgtest,
    piuparts es nuestra herramienta de pruebas de instalación, actualización y desinstalación
    y lintian es un comprobador extensivo para paquetes Debian.
  </dd>
</dl>

<br>

<h1>Lo que dicen nuestros usuarios</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      Para mí es el nivel perfecto de facilidad de uso y estabilidad. He usado
      varias distribuciones diferentes a lo largo de los años pero Debian es la única
      que simplemente funciona.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Sólido como una roca. Montones de paquetes. Excelente comunidad.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian para mí es símbolo de estabilidad y facilidad de uso.
    </strong></q>
  </li>
</ul>
