#use wml::debian::template title="Debian 介绍" MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c"

<a id=community></a>
<h2>Debian 是一个由普罗大众组成的社区</h2>
<p>来自世界各地的数千志愿者共同工作，注重自由软件和用户需求。</p>

<ul>
  <li>
    <a href="people">人员：</a>
    我们是谁，我们做什么
  </li>
  <li>
    <a href="philosophy">理念：</a>
    我们为什么要做，我们如何去做
  </li>
  <li>
    <a href="../devel/join/">参与其中：</a>
    你也能成为其中的一员！
  </li>
  <li>
    <a href="help">您如何协助 Debian？</a>
  </li>
  <li>
    <a href="../social_contract">社群契约：</a>
    我们的道德准则
  </li>
  <li>
    <a href="diversity">多样性声明</a>
  </li>
  <li>
    <a href="../code_of_conduct">行为准则</a>
  </li>
  <li>
    <a href="../partners/">合作伙伴：</a>
    为 Debian 项目提供持续协助的公司和组织
  </li>
  <li>
    <a href="../donations">捐赠</a>
  </li>
  <li>
    <a href="../legal/">法律信息</a>
  </li>
  <li>
    <a href="../legal/privacy">数据隐私</a>
  </li>
  <li>
    <a href="../contact">联系我们</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Debian 是自由的操作系统</h2>
<p>我们以 Linux 为基础，并添加了数以千计的应用程序以满足用户的需要。</p>

<ul>
  <li>
    <a href="../distrib">下载：</a>
    更多版本的 Debian 映像
  </li>
  <li>
  <a href="why_debian">选择 Debian 的理由</a>
  </li>
  <li>
    <a href="../support">支持：</a>
    获取帮助
  </li>
  <li>
    <a href="../security">安全</a>
    最近的更新 <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages">软件包：</a>
    搜索和浏览我们提供的众多软件的列表
  </li>
  <li>
    <a href="../doc">文档</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Debian 维基</a>
  </li>
  <li>
    <a href="../Bugs">缺陷报告</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    邮件列表</a>
  </li>
  <li>
    <a href="../blends"> Pure Blends：</a>
    满足特定需要的元软件包
  </li>
  <li>
    <a href="../devel">开发者天地</a>
    主要面向 Debian 开发者的信息
  </li>
  <li>
    <a href="../ports"> 移植/架构：</a>
    我们支持的 CPU 架构
  </li>
  <li>
    <a href="search">关于如何使用 Debian 搜索引擎的信息</a>。
  </li>
  <li>
    <a href="cn">关于多语言页面的信息</a>。
  </li>
</ul>
