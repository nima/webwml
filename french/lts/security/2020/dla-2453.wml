#use wml::debian::translation-check translation="297879a5fd7ae693e6b7a68efc86f356a3d3304e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>golang-go.crypto a été récemment mis à jour avec un correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-9283">CVE-2020-9283</a>.
Cela conduit à ce que tous les paquets utilisant le code affecté soient
recompilés pour tenir compte du correctif de sécurité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9283">CVE-2020-9283</a>

<p>La vérification de signature SSH pourrait causer une <q>panique</q>
lorsqu’une clé publique non valable est fournie.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.3.3-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets restic.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de restic, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/restic">https://security-tracker.debian.org/tracker/restic</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2453.data"
# $Id: $
