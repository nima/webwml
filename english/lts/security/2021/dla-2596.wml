<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in the shadow suite of login
tools. An attacker may escalate privileges in specific configurations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-20002">CVE-2017-20002</a>

    <p>Shadow incorrectly lists pts/0 and pts/1 as physical terminals in
    /etc/securetty. This allows local users to login as password-less
    users even if they are connected by non-physical means such as SSH
    (hence bypassing PAM's nullok_secure configuration). This notably
    affects environments such as virtual machines automatically
    generated with a default blank root password, allowing all local
    users to escalate privileges. It should be noted however that
    /etc/securetty will be dropped in Debian 11/bullseye.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12424">CVE-2017-12424</a>

    <p>The newusers tool could be made to manipulate internal data
    structures in ways unintended by the authors. Malformed input may
    lead to crashes (with a buffer overflow or other memory
    corruption) or other unspecified behaviors. This crosses a
    privilege boundary in, for example, certain web-hosting
    environments in which a Control Panel allows an unprivileged user
    account to create subaccounts.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:4.4-4.1+deb9u1.</p>

<p>We recommend that you upgrade your shadow packages.</p>

<p>For the detailed security status of shadow please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/shadow">https://security-tracker.debian.org/tracker/shadow</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2596.data"
# $Id: $
