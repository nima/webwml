#use wml::debian::blend title="Suporte do blend"
#use "navbar.inc"
#use wml::debian::translation-check translation="ac688ebb94c4334be47e6f542320d90001163462"

<h2>Documentação</h2>

<p>Antes de procurar alguém para suporte, é geralmente bom tentar
encontrar uma resposta para seu problema sozinho(a). Desse modo, você
conseguirá as respostas que precisa e, mesmo se não conseguir, a experiência
de ler a documentação provavelmente será útil para você no futuro.</p>

<p>Veja a <a href="./docs">página de documentação específica sobre rádio amador</a>
ou a <a href="../../doc/">página de documentação do Debian</a> para uma lista
de documentos disponíveis.</p>

<h2>Listas de discussão</h2>

<p>O Debian é aperfeiçoado através de desenvolvimento distribuído ao redor
do mundo. Portanto, o e-mail é o meio preferido para discussão de vários
itens. Muitas das conversas entre desenvolvedores(as) Debian e usuários(as)
são feitas através de listas de discussão.</p>

# Tradutores(as): a primeira lista de discussão é exclusiva em inglês. Você
# deve escolher se remove ou não a referência à esta lista

<p>Para suporte especificamente relacionado ao uso de software radioamadorista,
você pode usar a <a
href="https://lists.alioth.debian.org/mailman/listinfo/pkg-hamradio-user">lista
de discussão dos(as) usuários(as) de rádio amador</a>.</p>

# Tradutores(as): favor alterar a próxima lista para uma em idioma local.

<p>Para suporte geral ao Debian em português, você pode usar essa <a
href="https://lists.debian.org/debian-user-portuguese/">lista de discussão de usuários(as) do Debian</a>.</p>

<p>Para suporte ao(à) usuário(a) em outros idiomas, por favor verifique o <a
href="https://lists.debian.org/users.html">índice de listas de discussão para
usuários(as)</a>.</p>

<h2>Ajuda on-line e em tempo real usando IRC</h2>

<p>O <a href="http://www.irchelp.org/">IRC (bate-papo de retransmissão da
internet)</a> é uma forma de conversar com pessoas ao redor do planeta em
tempo real. Os canais IRC dedicados ao Debian podem ser encontrados em
<a href="http://www.oftc.net/">OFTC</a>.</p>

<p>Para conectar, você precisa de um cliente IRC. Alguns dos clientes mais
populares são
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> e
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
todos foram empacotados para o
Debian. Após instalar o cliente, você precisa informá-lo a se conectar com
o servidor. Na maioria dos clientes, pode-se fazer isso digitando:</p>

<pre>
/server irc.debian.org
</pre>

<p>Uma vez conectado(a), junte-se ao canal <code>#debian-hamchat</code>
digitando</p>

<pre>
/join #debian-hamchat
</pre>

<p>Nota: clientes como o HexChat geralmente têm uma interface gráfica diferente
para ingressar nos servidores/canais.</p>

<p>Existem diversas outras redes IRC onde também pode-se bater papo sobre o
Debian. Um dos mais proeminentes é a
<a href="https://freenode.net/">rede IRC freenode</a> em
<kbd>chat.freenode.net</kbd>.</p>
