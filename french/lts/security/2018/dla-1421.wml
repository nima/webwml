#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans l’interpréteur pour le langage
Ruby. Le projet « Common vulnérabilités et Exposures » (CVE) identifie les
problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9096">CVE-2015-9096</a>

<p>Injection de commande SMTP dans Net::SMTP à l’aide de séquences CRLF dans une
commande RCPT TO ou MAIL FROM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2339">CVE-2016-2339</a>

<p>Dépassement exploitable de tas dans Fiddle::Function.new.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7798">CVE-2016-7798</a>

<p>Traitement incorrect du vecteur d’initialisation dans le mode GCM dans
l’extension OpenSSL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0898">CVE-2017-0898</a>

<p>Vulnérabilité de sous-alimentation de tampon dans Kernel.sprintf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0899">CVE-2017-0899</a>

<p>Vulnérabilité de séquence d’échappement ANSI dans RubyGems.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0900">CVE-2017-0900</a>

<p>Vulnérabilité de déni de service dans la commande query de RubyGems.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0901">CVE-2017-0901</a>

<p>Installateur de gem permettant à un gem malveillant d’écraser des
fichiers arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0902">CVE-2017-0902</a>

<p>Vulnérabilité de détournement de requête DNS de RubyGems.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0903">CVE-2017-0903</a>

<p>Max Justicz a signalé que RubyGems est prédisposé à une désérialisation
d’objet non sûre. Lorsqu’analysée par une application qui traite des gems, une
spécification de gem formatée en YAML, contrefaite pour l'occasion, peut
conduire à une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10784">CVE-2017-10784</a>

<p>Yusuke Endoh a découvert une vulnérabilité d’injection de séquence
d’échappement dans l’authentification basique de WEBrick. Un attaquant peut
exploiter ce défaut pour injecter des séquences malveillantes dans le journal de
WEBrick et éventuellement exécuter des caractères de contrôle dans l’émulation
du terminal de la victime lors de la lecture de journaux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14033">CVE-2017-14033</a>

<p>Asac a signalé une vulnérabilité de sous-alimentation de tampon dans
l’extension OpenSSL. Un attaquant distant pourrait exploiter ce défaut pour
provoquer le plantage de l’interpréteur de Ruby aboutissant à un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14064">CVE-2017-14064</a>

<p>Divulgation de mémoire de tas dans la bibliothèque JSON.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17405">CVE-2017-17405</a>

<p>Vulnérabilité d’injection de commande dans Net::FTP pouvant permettre à un
serveur FTP malveillant d’exécuter des commandes arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>

<p>Aaron Patterson a signalé que WEBrick empaqueté avec Ruby était vulnérable
à une vulnérabilité de fractionnement de réponse HTTP. Il était possible pour un
attaquant d’injecter des réponses fausses si un script acceptait une entrée
externe et la sortait sans modification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17790">CVE-2017-17790</a>

<p>Vulnérabilité d’injection de commande dans lazy_initialze de lib/resolv.rb
pouvant permettre une attaque par injection de commande. Cependant, une entrée
non fiable est peu probable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6914">CVE-2018-6914</a>

<p>ooooooo_q a découvert une vulnérabilité de traversée de répertoires dans la
méthode Dir.mktmpdir de la bibliothèque tmpdir. Elle rendait possible à des
attaquants de créer des répertoires ou fichiers arbitraires l'aide d'un « .. »
(point point) dans l’argument du préfixe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8777">CVE-2018-8777</a>

<p>Eric Wong a signalé une vulnérabilité de déni de service par épuisement de
mémoire, relative à une large requête dans WEBrick empaqueté avec Ruby.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8778">CVE-2018-8778</a>

<p>aerodudrizzt a trouvé une vulnérabilité de lecture hors limites dans la
méthode String#unpack de Ruby. Si un grand nombre était passé avec le
spécificateur @, le nombre était traité comme une valeur négative et une lecture
hors limite se produisait. Des attaquants pourraient lire des données de tas
si un script acceptait une entrée externe comme argument de String#unpack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8779">CVE-2018-8779</a>

<p>ooooooo_q a signalé que les méthodes UNIXServer.open et UNIXSocket.open de la
bibliothèque de socket empaquetée avec Ruby ne vérifiait pas pour des octets NUL
dans l’argument du chemin. Le manque de vérification rend les méthodes
vulnérables à la création involontaire de socket et à l’accès involontaire de
socket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8780">CVE-2018-8780</a>

<p>ooooooo_q a découvert une traversée de répertoires involontaire dans quelques
méthodes dans Dir, à cause du manque de vérification pour des octets NUL dans
leurs paramètres.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000075">CVE-2018-1000075</a>

<p>Vulnérabilité de taille négative dans l’en-tête de paquet gem ruby pouvant
causer une boucle infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000076">CVE-2018-1000076</a>

<p>Le paquet RubyGems vérifie improprement les signatures chiffrées. Un gem
signé incorrectement pourrait être installé si l’archive contient plusieurs
signatures de gem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000077">CVE-2018-1000077</a>

<p>Vulnérabilité de validation incorrecte d’entrée dans l’attribut de la page
d’accueil de la spécification RubyGems pouvant permettre à un gem malveillant
de définir une URL de page d’accueil non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000078">CVE-2018-1000078</a>

<p>Vulnérabilité de script intersite (XSS) dans l’affichage du serveur de gem
de l’attribut de page d’accueil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000079">CVE-2018-1000079</a>

<p>Vulnérabilité de traversée de répertoires lors de l’installation de gem.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.1.5-2+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ruby2.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1421.data"
# $Id: $
