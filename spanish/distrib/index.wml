#use wml::debian::template title="Obtener Debian"
#use wml::debian::translation-check translation="128241c9727552589d968997e05c3557e016c9c6"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Debian se distribuye <a href="../intro/free">libremente</a>
a través de Internet. Puede descargarla completamente desde cualquiera de nuestras
<a href="ftplist">réplicas</a>.
El <a href="../releases/stable/installmanual">Manual de instalación</a>
contiene instrucciones de instalación detalladas.
Y las notas de publicación pueden encontrarse <a href="../releases/stable/releasenotes">aquí</a>.
</p>
<p>Esta página contiene opciones para la instalación de Debian «estable». Si lo que le interesa es la distribución «en pruebas»
   o «inestable», visite nuestra <a href="../releases/">página de versiones</a>.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Descargar una imagen de instalación</a></h2>
    <p>Dependiendo de su conexión a Internet puede descargar cualquiera de las siguientes opciones:</p>
    <ul>  
     <li>Una <a href="netinst"><strong>imagen de instalación pequeña</strong></a>:
	    se puede descargar rápidamente y debe guardarse en un disco extraíble.
	    Para utilizar esta opción debe tener una máquina con conexión a Internet
	    
<ul class="quicklist downlist">
<li><a title="Descargar el instalador para PC de 64 bits Intel y AMD"
      href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">iso netinst para
	      PC de 64 bits</a></li>
	  <li><a title="Descargar el instalador para PC de 32 bits Intel y AMD"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">iso netinst para
	      PC de 32 bits</a></li>
	</ul>
      </li>
      <li>Una <a href="../CD/"><strong>imagen de instalación
      completa</strong></a>: contiene más paquetes, haciendo más fácil la instalación en máquinas
	sin conexión a Internet.
	<ul class="quicklist downlist">
	  <li><a title="Descargar torrents del DVD para PC de 64 bits Intel y AMD"
	         href="<stable-images-url/>/amd64/bt-dvd/">torrents para PC de 64 bits (DVD)</a></li>
	  <li><a title="Descargar torrents del DVD para PC de 32 bits Intel y AMD"
		 href="<stable-images-url/>/i386/bt-dvd/">torrents para PC de 32 bits (DVD)</a></li>
	  <li><a title="Descargar torrents del CD para PC de 64 bits Intel y AMD"
	         href="<stable-images-url/>/amd64/bt-cd/">torrents para PC de 64 bits (CD)</a></li>
	  <li><a title="Descargar torrents del CD para PC de 32 bits Intel y AMD"
		 href="<stable-images-url/>/i386/bt-cd/">torrents para PC de 32 bits (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Use una imagen de Debian para la nube</a></h2>
    <ul>
      <li>Una <a href="https://cloud.debian.org/images/cloud/"><strong>imagen oficial para la nube</strong></a>:
            puede usarse directamente en su proveedor de nube, y está construida por el equipo de Debian para la nube («Debian Cloud Team»).
        <ul class="quicklist downlist">
          <li><a title="Imagen Qcow2 para Intel y AMD de 64 bits en OpenStack" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2">AMD/Intel de 64 bits en OpenStack (Qcow2)</a></li>
          <li><a title="Imagen Qcow2 para ARM de 64 bits en OpenStack" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-arm64.qcow2">ARM de 64 bits en OpenStack (Qcow2)</a></li>
        </ul>
      </li>
     </ul>
     <h2><a href="../CD/live/">Pruebe Debian live antes de instalar</a></h2>
     <p>
      Puede probar Debian arrancando un sistema «en vivo» desde un CD, DVD o USB sin 
      instalar ningún fichero en la máquina. Cuando esté listo puede ejecutar el 
      instalador incluido (a partir de Debian 10 Buster, este es el
      <a href="https://calamares.io">instalador Calamares</a>, un instalador fácil de usar por el usuario final).
      Este método puede ser el adecuado para usted siempre y cuando las imágenes 
      cumplan con sus requisitos de tamaño, lenguaje y selección de paquetes.
      Lea más <a href="../CD/live#choose_live">información sobre este método</a>
      para que lo ayude en su decisión.
   </p>
<ul class="quicklist downlist">
<li><a  title="Descargar torrents de la versión live para PC de 64 bits Intel y AMD"
       href="<live-images-url/>/amd64/bt-hybrid/">torrents para PC de 64 bits</a></li>
<li><a title="Descargar torrents de la versión live para PC de 32 bits Intel y AMD"
       href="<live-images-url/>/i386/bt-hybrid/">torrents para PC de 32 bits</a></li>
</ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Adquirir un juego de CD o DVD de uno de los vendedores
    de CD de Debian</a></h2>

   <p>
      Muchos de los vendedores ofrecen la distribución por menos de 5 US$
      más gastos de envío (compruebe en sus
      respectivas páginas que también realicen envíos internacionales).
      <br />
      Algunos de los <a href="../doc/books">libros sobre Debian</a>
      incluyen también CD.
   </p>

   <p>Las ventajas básicas de los CD son:</p>

   <ul>
     <li>La instalación desde CD es más directa.</li>
     <li>Puede instalarse en máquinas sin conexión a Internet.</li>
     <li>Puede instalar Debian (en tantas máquinas como quiera) sin descargar 
     los paquetes por sí mismo.</li>
     <li>El CD se puede usar para recuperar con más facilidad un sistema Debian
         dañado.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">Compre una máquina con Debian
      preinstalado</a></h2>
   <p>Esto tiene una serie de ventajas:</p>
   <ul>
    <li>No tiene que instalar Debian.</li>
    <li>La instalación está preconfigurada para ajustarse al
    hardware.</li>
    <li>Puede que el fabricante proporcione soporte técnico.</li>
   </ul>
  </div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Si algún componente hardware de su sistema <strong>requiere cargar «firmware»
no libre</strong> con el controlador de dispositivo, puede usar uno de los
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archivos comprimidos con paquetes de «firmware» común</a> o descargar una imagen <strong>no oficial</strong>
que incluya estos «firmwares» <strong>no libres</strong>. En la <a href="../releases/stable/amd64/ch06s04">guía de instalación</a> puede encontrar
instrucciones sobre cómo usar los archivos comprimidos e información general sobre cómo cargar
el «firmware» durante la instalación.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">imágenes
no oficiales de instalación de <q>estable</q> con «firmware» incluido</a>
</p>
</div>
