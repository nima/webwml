#
# Damyan Ivanov <dmn@debian.org>, 2011-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.51\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-12-17 14:55+0200\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@ludost.net>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 3.38.0\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "възлагателно писмо"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "писмо за назначаване"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "делегат"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "делегат"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "той/него"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "тя/нея"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "делегат"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "те/тях"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "текущ"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "член"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "отговорник"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "УСИ"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Управител на стабилното издание"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "магьосник"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "председател"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "асистент"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "секретар"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "представител"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "роля"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"Позициите в долния списък, означени с <q>настоящ</q> са или изборни, или "
"назначавани с ограничен срок."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Официални длъжности"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Дистрибуция"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr "Комуникации и връзки"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr "Екип за защита на данните"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "Публичност"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr "Членство в други организации"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Поддръжка и инфраструктура"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Лидер"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Техническа комисия"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Секретар"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Разработка"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP архиви"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "Магьосник на FTP"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Асистенти"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "Магьосници на FTP"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Обновени пакети за стабилното издание"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Екип за поддържане на обновените пакети за стабилното издание"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Управление на изданията"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Екип по изданията"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Контрол на качеството"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Екип по инсталационната система"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr "Екип Debian Live"

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Бележки по изданията"

#: ../../english/intro/organization.data:152
msgid "CD/DVD/USB Images"
msgstr "Инсталационни носители"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Производство"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Тестване"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr "Облачни технологии"

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "Инфраструктура за автоматично компилиране на пакети"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "Екип на wanna-build"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Администриране на buildd"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Документация"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "Списък на незавършени и планирани пакети"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Контакт с пресата"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Уеб-страници"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Планета Дебиан"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr "Връзки"

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Проект Жени в Дебиан"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr "Грижа за общността"

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"За да изпратите шифровано съобщение до всички членове на екипа за грижа за "
"общността използвайте GPG ключ 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Събития"

#: ../../english/intro/organization.data:264
msgid "DebConf Committee"
msgstr "Организиране на конференции"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Партньоркса програма"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Координация на хардуерни дарения"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr "Фондация ГНОМ"

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Система за следене на грешки"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Администрация и архив на пощенските списъци"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "Портал за кандидат-членове"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Администратори на членството в проекта"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"За да изпратите шифровано съобщение до всички администратори за членство в "
"проекта използвайте GPG ключ 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Отговорници за ключодържателите (PGP и GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Екип по сигурността"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Правилник"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Системна администрация"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Това е адресът, на който да съобщите при проблеми с някоя от машините на "
"Дебиан, включително проблеми с паролата или липса на пакети."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Ако имате хардуерни проблеми с някоя от машините на Дебиан, прочетете "
"страницата <a href=\"https://db.debian.org/machines.cgi\">Машини на Дебиан</"
"a>, която може да съдържа информация за администратора на конкретната машина."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "Администратор на директорията на разработчиците в LDAP"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Огледални сървъри"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "Отговорник за DNS"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Система за следене на пакетите"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr "Ковчежник"

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"запитвания за използването на <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">търговските марки</a>"

#: ../../english/intro/organization.data:392
msgid "Salsa administrators"
msgstr "Администратори на Salsa"

#~ msgid "Ports"
#~ msgstr "Архитектури"

#~ msgid "Special Configurations"
#~ msgstr "Специални конфигурации"

#~ msgid "Laptops"
#~ msgstr "Преносими компютри"

#~ msgid "Firewalls"
#~ msgstr "Защитни стени"

#~ msgid "Embedded systems"
#~ msgstr "Системи за вграждане"

#~ msgid "User support"
#~ msgstr "Поддръжка за потребители"

#~ msgid "Individual Packages"
#~ msgstr "Отделни пакети"

#~ msgid "Live System Team"
#~ msgstr "Екип на живата система"

#~ msgid "Auditor"
#~ msgstr "Одитор"

#~ msgid "Publicity"
#~ msgstr "Публичност"

#~ msgid "Bits from Debian"
#~ msgstr "Кратки новини за Дебиан"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Отговорници за ключодържателите (DM)"

#~ msgid "DebConf chairs"
#~ msgstr "Ръководство на ДебКонф"

#~ msgid "Testing Security Team"
#~ msgstr "Екип по сигурността на тестовата дистрибуция"

#~ msgid "Security Audit Project"
#~ msgstr "Проект за одит на сигурността"

#~ msgid "Volatile Team"
#~ msgstr "Екип на volatile"

#~ msgid "Vendors"
#~ msgstr "Търговци"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Дебиан за организации с идеална цел"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Универсалната операционна система"

#~ msgid "Accountant"
#~ msgstr "Счетоводител"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Имената на отделните администратори на buildd могат да бъдат открити на "
#~ "<a href=\"http://www.buildd.net\">http://www.buildd.net</a>. Изберете "
#~ "артхитектура и дистрибуция за да видите наличните buildd и техните "
#~ "администратори."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Администраторите отговорни за buildd на дадена архитектура могат да бъдат "
#~ "открити на <genericemail архитекрура@buildd.debian.org>, например "
#~ "<genericemail i386@buildd.debian.org>."

#~ msgid "APT Team"
#~ msgstr "Екип APT"

#~ msgid "Release Assistants"
#~ msgstr "Асистенти"

#~ msgid "Release Managers for ``stable''"
#~ msgstr "Управители на стабилните идания"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Специализирани дистрибуции"

#~ msgid "Key Signing Coordination"
#~ msgstr "Координация на подписване на ключовете"

#~ msgid "Marketing Team"
#~ msgstr "Маркетингов екип"

#~ msgid "Handhelds"
#~ msgstr "Джобни компютри"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (Неактивна, не е част от изданието squeeze)"

#~ msgid "Summer of Code 2013 Administrators"
#~ msgstr "Отговорници за програмата „Програмиране през лятото 2013“"

#~ msgid "current Debian Project Leader"
#~ msgstr "текущ лидер на проекта"

#~ msgid "Alioth administrators"
#~ msgstr "Администратори на Alioth"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Дебиан за образователни цели"

#~ msgid "Debian for science and related research"
#~ msgstr "Дебиан за научни изследвания"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Дебиан за хора с увреждания"

#~ msgid "Debian in legal offices"
#~ msgstr "Дебиан за правни кантори"

#~ msgid "Debian for education"
#~ msgstr "Дебиан за образователни цели"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Дебиан за медицински практики и разработки"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Дебиан за деца от 1 до 99г."

#~ msgid "Debian Pure Blends"
#~ msgstr "Дестилати на Дебиан"

#~ msgid "Anti-harassment"
#~ msgstr "Без тормоз"

#~ msgid "CD Vendors Page"
#~ msgstr "Страница на търговците на компактдискове"

#~ msgid "Consultants Page"
#~ msgstr "Страница на консултантите"
