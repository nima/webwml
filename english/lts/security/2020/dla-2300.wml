<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue where kdepim-runtime would default
to using unencrypted POP3 communication despite the UI indicating that
encryption was in use.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15954">CVE-2020-15954</a>

    <p>KDE KMail 19.12.3 (aka 5.13.3) engages in unencrypted POP3 communication
    during times when the UI indicates that encryption is in use.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
4:16.04.2-2+deb9u1.</p>

<p>We recommend that you upgrade your kdepim-runtime packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2300.data"
# $Id: $
