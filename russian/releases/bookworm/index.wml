#use wml::debian::template title="Информация о выпуске Debian &ldquo;bookworm&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="43ed34ae31e2b37b42af378b2dcc661486ce5646" maintainer="Lev Lamberov"

<if-stable-release release="bookworm">

<p>Debian <current_release_bookworm> был
выпущен <a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "Debian 12.0 изначально был выпущен <:=spokendate('XXXXXXXX'):>."
/>
Выпуск включает множество важных
изменений, описанных в
нашем <a href="$(HOME)/News/XXXX/XXXXXXXX">анонсе</a> и
в <a href="releasenotes">информации о выпуске</a>.</p>

#<p><strong>Debian 12 был заменён на
#<a href="../trixie/">Debian 13 (<q>trixie</q>)</a>.
#Обновления безопасности прекращены с <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Тем не менее bullseye получает долгосрочную поддержку (LTS) вплоть до
#конца xxxxx 20xx. LTS ограничиается архитектурами i386, amd64, armel, armhf и arm64.
#Все остальные архитектуры более не поддерживаются в bookworm.
#Дополнительную информацию см. в <a
#href="https://wiki.debian.org/LTS">разделе LTS вики Debian</a>.
#</strong></p>

<p>О том, как получить и установить Debian, см. страницу с
<a href="debian-installer/">информацией по установке</a> и
<a href="installmanual">руководство по установке</a>. Инструкции
по обновлению со старого выпуска см. в
<a href="releasenotes">информации о выпуске</a>.</p>

### Activate the following when LTS period starts.
#<p>Архитектуры, поддерживаемые в ходе жизненного цикла LTS:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Изначально в выпуске bookworm поддерживались следующие архиектуры:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Независимо от нашего желания в выпуске могут быть некоторые проблемы, несмотря на то, что он объявлен
<em>стабильным</em>. Мы составили
<a href="errata">список основных известных проблем</a>, и вы всегда можете сообщить нам
<a href="reportingbugs">о других ошибках</a>.</p>

<p>Наконец, мы составили список <a href="credits">людей, которые внесли свой вклад</a>
в создание этого выпуска.</p>
</if-stable-release>

<if-stable-release release="bullseye">

<p>Следующий выпуск после <a
href="../bullseye /">bullseye</a> называется <q>bookworm</q>.</p>

<p>Начальная версия этого выпуска представляла собой копию bookworm, и сейчас он находится в стадии
<q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">тестирования</a></q>.
Это означает, что сейчас ничего не должно сломаться, как это бывает в нестабильном
или экспериментальном дистрибутиве, так как пакеты попадают в дистрибутив только
после определённого периода, и если они не содержат критических для всего выпуска
ошибок.</p>

<p>Заметим, что обновления безопасности для <q>тестируемого</q> дистрибутива ещё
<strong>не</strong> поддерживаются командой безопасности. Следовательно, <q>тестируемый</q> выпуск
<strong>не</strong> получает обновлений безопасности своевременно.
# Подробный отчет об этом см. в
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">анонсе</a>
# команды безопасности тестируемого выпуска.
Если вам требуется поддержка безопасности, то
пока вам лучше изменить записи в sources.list с testing на bullseye. Также
см. пункт в
<a href="$(HOME)/security/faq#testing">ЧаВО команды безопасности</a> о <q>тестируемом</q>
выпуске.</p>

<p>Возможно, доступен <a href="releasenotes">черновой вариант информации о выпуске</a>.
Также посмотрите <a href="https://bugs.debian.org/release-notes">предложенные
обновления к информации о выпуске</a>.</p>

<p>Установочные образы и инструкции по установке <q>тестируемого</q> выпуска
см. на <a href="$(HOME)/devel/debian-installer/">странице Debian-Installer</a>.</p>

<p>Подробней о работе <q>тестируемого</q> выпуска см.
<a href="$(HOME)/devel/testing">информацию от разработчиков</a>.</p>

<p>Люди часто интересуются об <q>индикаторе готовности</q> определённого дистрибутива.
К сожалению его нет, но вот несколько мест, в которых описаны вещи, которые должны
быть выполнены для того, чтобы выпуск состоялся:</p>

<ul>
  <li><a href="https://release.debian.org/">Страница общего состояния выпуска</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Критичные для выпуска ошибки</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Ошибки в базовой части</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Ошибки в стандартных пакетах и пакетах задач</a></li>
</ul>

<p>Также отчёты об общем состоянии публикуются ответственным за выпуск в
<a href="https://lists.debian.org/debian-devel-announce/">списке рассылки
debian-devel-announce</a>.</p>

</if-stable-release>

<if-stable-release release="buster">

<p>Кодовое имя для следующего крупного выпуска Debian после <a
href="../bullseye /">bullseye</a> &mdash; <q>bookworm</q>. В настоящее
время <q>bullseye</q> ещё не выпущен. Поэтому выпуск <q>bookworm</q> всё ещё
очень далеко.</p>

</if-stable-release>
