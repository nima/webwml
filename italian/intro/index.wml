#use wml::debian::template title="Introduzione a Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c"

<a id=community></a>
<h2>Debian è un comunità di persone</h2>
<p>Migliaia di volontari di ogni parte del mondo lavorano assieme
 dando priorità al Software Libero e alle necessità degli utenti.</p>

<ul>
  <li>
    <a href="people">Le persone:</a>
    Chi siamo e cosa facciamo
  </li>
  <li>
    <a href="philosophy">Filosofia:</a>
    Perché e come lo facciamo
  </li>
  <li>
    <a href="../devel/join/">Partecipa:</a>
    Puoi entrare nella comunità!
  </li>
  <li>
    <a href="help">Come posso aiutare?</a>
  </li>
  <li>
    <a href="../social_contract">Contratto sociale:</a>
    I nostri principi
  </li>
  <li>
    <a href="diversity">Dichiarazione sulla diversità</a>
  </li>
  <li>
    <a href="../code_of_conduct">Codice di condotta</a>
  </li>
  <li>
    <a href="../partners/">Partner:</a>
    Aziende e organizzazioni che forniscono un sostegno costante
    al progetto Debian
  </li>
  <li>
    <a href="../donations">Donazioni</a>
  </li>
  <li>
    <a href="../legal/">Informazioni legali</a>
  </li>
  <li>
    <a href="../legal/privacy">Privacy dei dati</a>
  </li>
  <li>
    <a href="../contact">Contatti</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Debian è un Sistema Operativo Libero</h2>
<p>Partiamo da Linux e aggiungiamo parecchie migliaia di applicazioni
per soddisfare le necessità dei nostri utenti.</p>

<ul>
  <li>
    <a href="../distrib">Scarica:</a>
    Altre versioni delle immagini Debian
  </li>
  <li>
  <a href="why_debian">Perché Debian</a>
  </li>
  <li>
    <a href="../support">Assistenza:</a>
    Ricevere aiuto
  </li>
  <li>
    <a href="../security">Sicurezza:</a>
    Ultimi aggiornamenti <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages">Pacchetti software:</a>
    Fai ricerche e naviga nel lungo elenco dei nostri software
  </li>
  <li>
    <a href="../doc">Documentazione</a>
  </li>
  <li>
    <a href="https://wiki.debian.org">Debian wiki</a>
  </li>
  <li>
    <a href="../Bugs">Segnalazione di bug</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
	Liste di messaggi</a>
  </li>
  <li>
    <a href="../blends">Pure Blends:</a>
    Metapacchetti per usi specifici
  </li>
  <li>
    <a href="../devel">Angolo degli sviluppatori:</a>
    Informazioni rivolte principalmente agli sviluppatori Debian.
  </li>
  <li>
    <a href="../ports">Port/Architetture:</a>
    Computer su cui funziona il sistema
  </li>
  <li>
    <a href="search">Informazioni su come usare il motore di ricerca Debian</a>.
  </li>
  <li>
    <a href="cn">Informazioni sulle pagine disponibili in più langue</a>.
  </li>
</ul>
