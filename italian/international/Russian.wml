#use wml::debian::template title="Comunità Debian di lingua russa"
#use wml::debian::toc
#use wml::debian::translation-check translation="c0816ff3dae7deb1cd48fb2f83fa934f5e5f3559" maintainer="skizzhg"

<p>In questa pagina troverete tutto ciò che riguarda gli utenti russi di
Debian. Se si ritiene che alcune informazioni siano mancanti, si prega di
segnalarlo sul <a href="#site">sito internet del team di traduzione</a>.</p>

<toc-display/>

<toc-add-entry name="users">Gruppo utenti</toc-add-entry>

<p>I principali luoghi d'incontro degli utenti russi di Debian
sono la mailing list
<a href="https://lists.debian.org/debian-russian">debian-russian</a> 
e il canale IRC #debian-russian su irc.debian.org. Se avete domande su
Debian o Linux in generale (anche se preferiamo domande su Debian) potete
porle qui. Si prega di non inviare messaggi in russo su
<a href="https://lists.debian.org/debian-user">debian-user</a> e altre
mailing list Debian dove il russo non è parlato, molto probabilmente non
otterrete la risposta che vi aspettate. Solo due mailing list Debian sono
in lingua russa:
<a href="https://lists.debian.org/debian-russian">debian-russian</a> e <a
href="https://lists.debian.org/debian-l10n-russian">debian-l10n-russian</a>.</p>

<toc-add-entry name="l10n">Team di localizzazione</toc-add-entry>

<p><a href="https://wiki.debian.org/ru/L10n/Russian">Il team di
localizzazione</a> è responsabile per la localizzazione di software
incluso in Debian, la sua mailing list è <a
href="https://lists.debian.org/debian-l10n-russian">debian-l10n-russian</a>.

<toc-add-entry name="docs">Traduzione russa della documentazione</toc-add-entry>

<p>Sono qui elencati i documenti Debian tradotti in russo. Se siete a
conoscenza di eventuali traduzioni non elencate, si prega di segnalarlo
su <a href="https://lists.debian.org/debian-l10n-russian">mailing list
debian-l10n-russian</a>.</p>

<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Manuale d'installazione per l'attuale 
Debian stable</a></li>
<li><a href="$(HOME)/releases/stable/releasenotes">eNote di rilascio per l'attuale
Debian stable</a></li>
<li><a href="$(HOME)/doc/user-manuals#faq">The Debian GNU/Linux FAQ</a></li>
<li><a href="$(HOME)/doc/user-manuals#refcard">Debian
GNU/Linux Reference Card</a></li>
<li><a href="$(HOME)/doc/misc-manuals#history">Breve storia di Debian</a></li>
<li><a href="$(HOME)/doc/devel-manuals#maint-guide">Guida per il nuovo
maintainer</a></li>
<li><a href="$(HOME)/doc/misc-manuals#packaging-tutorial">
Tutorial sulla pacchettizzazione</a></li>
</ul>

<toc-add-entry name="obsolete-docs">Traduzioni russe di documentazione obsoleta</toc-add-entry>

<p>La documentazione Debian obsoleta tradotta in Russo è elencata qui.
Se sei a conoscenza di qualche materiale che potrebbe essere aggiunto non esitare a segnalarcelo a questa <a
href="https://lists.debian.org/debian-l10n-russian">mailing list
debian-l10n-russian</a>.</p>

<ul>
<li><a href="$(HOME)/doc/obsolete#dselect">Guida a dselect
per principianti</a></li>
<li><a href="$(HOME)/doc/obsolete#apt-howto">HOWTO su APT</a></li>
</ul>
