#use wml::debian::submenu
#include "$(ENGLISHDIR)/ports/menu.defs"


<define-menu-item name="netbsd-i386">
  <gettext domain="ports">Debian GNU/NetBSD para i386</gettext>
</define-menu-item>

<define-menu-item name="netbsd-alpha">
  <gettext domain="ports">Debian GNU/NetBSD para Alpha</gettext>
</define-menu-item>

<define-menu-item name="why">
  <gettext domain="ports">Por que</gettext>
</define-menu-item>

<define-menu-item name="people">
  <gettext domain="ports">As pessoas</gettext>
</define-menu-item>

<menu-item name="ports"         basename="../index">
<menu-item name="netbsd-i386"   basename="index">
<menu-item name="netbsd-alpha"  basename="alpha">
<menu-item name="news"          basename="news">
<menu-item name="why"           basename="why">
<menu-item name="people"	basename="people">
