#use wml::debian::cdimage title="데비안 CD/DVD 이미지를 HTTP/FTP를 통해 다운로드" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="9067c7c4cd42820e6b539f78e067c9e725ca6f9f" maintainer="Seunghun Han (kkamagui)"

<div class="tip">
<p><strong>웹 브라우저에서 CD나 DVD 이미지를 다른 파일 다운로드하듯이 다운로드하지
마세요!</strong>
내려받다가 끊어지면 대부분의 브라우저가 끊어진 지점에 이어서 다운로드를 시작하지
않기 때문입니다.
</p>
</div>

<p>대신, 이어받기 기능이 있는 도구를 쓰세요. 유닉스 환경에서는
<a href="http://aria2.sourceforge.net/">aria2</a>나
<a href="http://dfast.sourceforge.net/">wxDownload Fast</a>를 쓰거나 명령행에서
<q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> 또는
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>를
쓸 수 있습니다.
윈도우즈 환경에서는 아마
<a href="https://www.freedownloadmanager.org/">Free Download Manager</a>를 쓰고
싶을지도 모릅니다.
<a href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">다운로드
매니저(Download Manager) 비교</a>를 살펴보고 여러분이 마음에 드는 것을 선택할
수도 있죠.</p>

<p>아래의 데비안 이미지를 다운로드할 수 있습니다:</p>

<ul>

  <li><a href="#stable"><q>안정(stable)</q> 릴리스의 공식 CD/DVD 이미지</a></li>

  <li><em>매주마다 다시 만드는</em> <q>테스트(testing)</q> 배포판의
  <a href="https://cdimage.debian.org/cdimage/weekly-builds/">공식
  CD/DVD 이미지</a></li>

<comment>
  <li><q>테스트(Testing)</q> 및 <q>불안정(unstable)</q> 배포판의 비공식
  CD/DVD 이미지는 fsn://HU &mdash; <a href="#unofficial">이 페이지의 아래에</a>
  있습니다.</li>
</comment>

</ul>

<p>아래도 함께 보세요:</p>
<ul>

  <li><tt>debian-cd/</tt> 미러 서버의 <a href="#mirrors">전체 목록</a></li>

  <li><q>네트워크 설치</q> (150-300&nbsp;MB) 이미지는
  <a href="../netinst/">네트워크 설치</a> 페이지를 보세요.</li>

  <li>동작하는 스냅샷으로 불리는 <q>테스트(testing)</q> 릴리스의
  <q>netinst</q> 이미지는 <a href="$(DEVEL)/debian-installer/">데비안 설치관리자
  페이지</a>를 보세요.</li>

</ul>

<hr />

<h2><a name="stable"><q>안정(stable)</q> 릴리스의 공식 CD/DVD 이미지</a></h2>

<p>데비안을 인터넷 연결 없이 컴퓨터에 설치하려면,
CD(각각 700&nbsp;MB)나 DVD 이미지(각각 4.7&nbsp;GB)를 사용할 수 있습니다.
첫 CD 또는 DVD 이미지 파일을 다운로드하고, CD/DVD recorder에 쓰거나 i386과 amd64
시스템이라면 USB 스틱에다 쓰고, 재시작하세요.
</p>

<p><strong>첫</strong> CD/DVD에는 표준 데비안 시스템을
설치하는 데 필요한 모든 파일이 들어있습니다.
<br />
불필요한 다운로드를 피하려면, 여러분이 패키지가 필요할 때까지 다른 CD나
DVD 이미지를 다운로드하지 <strong>마세요</strong>.
</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>아래 링크는 크기가 최대 650&nbsp;MB인 파일을 가리키며,
일반 CD-R(W) 미디어에 적합합니다:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>아래 링크는 크기가 최대 4.4&nbsp;GB인 파일을 가리키며,
일반 DVD-R/DVD+R 및 비슷한 미디어에 적합합니다:
</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>설치 전에 문서를 보세요.
설치 전에 <strong>문서를 딱 하나만 읽는다면
</strong>, 
<a href="$(HOME)/releases/stable/i386/apa">설치 방법</a>를
읽고, 설치 과정을 간략히 훑으세요.
다른 쓸만한 문서는 아래에 있습니다:
</p>
<ul>
<li>상세한 설치 방법이 들어 있는
    <a href="$(HOME)/releases/stable/installmanual">설치 안내서</a></li>
<li>자주 묻는 질문(FAQ)이 들어 있는
    <a href="https://wiki.debian.org/DebianInstaller">데비안 설치 문서</a></li>
<li>설치 시 이미 알려진 문제가 들어있는
    <a href="$(HOME)/releases/stable/debian-installer/#errata">데비안
    설치 오류들</a></li>
</ul>

<hr />

<h2><a name="mirrors"><q>데비안 CD(debian-cd)</q> 저장소에 등록된
미러 서버</a></h2>

<p>주의하세요. <strong>미러 서버의 일부는 최신 상태가 아닙니다</strong> &mdash;
다운로드 하기 전에 이미지 목록이 <a href="../#latest">현재 사이트</a>의 목록과
같은지 체크하세요!
한 마디 덧붙이자면, 이미지 용량, 특히 DVD 이미지 때문에 전체 사이트를
복제하지 못하는 사이트가 많다는 것도 주의하세요. </p>

<p><strong>만약 의심스럽다면, 스웨덴에 있는
<a href="https://cdimage.debian.org/debian-cd/">주 CD 이미지 서버</a></strong>를
사용하거나, 여러분 주변에 현재 버전이 있는 미러 서버로 자동 연결해주는
<a href="http://debian-cd.debian.net/">실험적 자동 미러 서버 선택 서버</a>를 사용하세요.</p>

<p> 여러분의 미러 서버를 이용해서 데비안의 CD 이미지를 제공하고 싶으세요?
그렇다면 <a href="../mirroring/">CD 이미지 미러 서버를 설정하는 방법</a>을
보세요.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"


<comment>
<h2><a name="unofficial"> <q>테스트(testing)</q> 및
<q>불안정(Unstable)</q> 릴리스와 관련된 비공식 CD/DVD 이미지</a></h2>

<p>테스트 및 불안정 이미지는 데비안이 빌드하고 제공하는 것이 아니라
<a href="http://www.fsn.hu/">fsn://HU</a>에서 제공합니다:</p>

<ul>

  <li>fsn://HU에서 제공하는 <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/etch/">\
  <q>테스트(testing)</q> 배포판</a> CD(<em>amd64와 i386용으로
  매주 빌드됨</em>)와
  <a href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/MIRRORS">\
  미러 서버</a></li>

  <li>fsn://HU에서 제공하는 <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/etch-dvd/">\
  <q>테스트(testing)</q> 배포판</a> DVD (<em>amd64와 i386용으로
  매주 빌드됨</em>)와
  <a href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/MIRRORS">\
  미러 서버</a></li>

  <li>fsn://HU에서 제공하는 <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/sid/">\
  <q>불안정(unstable)</q> 배포판</a> CD (<em>amd64와 i386용으로
  매주 빌드됨</em>),
  <a href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/MIRRORS">\
  미러 서버</a></li>

  <li>fsn://HU에서 제공하는 <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/sid-dvd/">\
  <q>불안정(unstable)</q> 배포판</a> DVD (<em>amd64와 i386용으로
  매주 빌드됨</em>),
  <a href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/MIRRORS">\
  미러 서버</a></li>

</ul>
</comment>
