#use wml::debian::translation-check translation="35022c48a0167d79ad83f69c33eea08bda088d57" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'interpréteur pour le
langage Ruby.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10663">CVE-2020-10663</a>

<p>Jeremy Evans a signalé une vulnérabilité de création d'objet non sûr
dans le module (« gem ») JSON empaqueté avec Ruby. Lors de l'analyse de
certains documents JSON, le module JSON peut être contraint à créer des
objets arbitraires dans le système cible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10933">CVE-2020-10933</a>

<p>Samuel Williams a signalé un défaut dans la bibliothèque de socket qui
pourrait conduire à une fuite de données éventuellement sensibles issues de
l'interpréteur.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.5.5-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby2.5.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby2.5, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby2.5">\
https://security-tracker.debian.org/tracker/ruby2.5</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4721.data"
# $Id: $
