#use wml::debian::cdimage title="从最小光盘进行网络安装"
#use wml::debian::release_info
#use wml::debian::installer
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"
#include "$(ENGLISHDIR)/releases/images.data"

<p><q>网络安装</q>或<q>netinst CD</q>是单张光盘，可以\
安装整个操作系统。 这张光盘只包含\
最少的软件以开始安装，并\
通过因特网获取剩余的软件包。</p>

<p><strong>对我来说哪个更好 &mdash; 最小可启动 CD-ROM 还是\
完整的光盘？</strong>这不一定，但是我们认为在大多数情况下，\
最小光盘映像更好 &mdash; 首先，您只需下载\
您选择安装在机器上的软件包，从而节省\
时间和带宽。 另一方面，当在多台机器上或在没有\
免费因特网连接的机器上安装时，完整的光盘更为\
合适。</p>

<p><strong>在安装过程中支持哪些类型的\
网络连接？</strong>
网络安装假定您已连接到了\
因特网。支持各种不同的方式，如\
模拟 PPP 拨号、以太网、WLAN（有一些限制），但是
ISDN 并不在此列 &mdash; 抱歉！</p>

<p>以下最小可启动光盘映像可供\
下载：</p>

<ul>

  <li><q>稳定（stable）</q>版本的官方<q>网络安装</q>映像 &mdash; <a
  href="#netinst-stable">见下</a></li>

  <li><q>测试（testing）</q>版本的映像，包含每日构建以及已确认\
可工作的快照，参见 <a
  href="$(DEVEL)/debian-installer/">Debian 安装程序页面</a>。</li>

</ul>


<h2 id="netinst-stable"><q>稳定（stable）</q>版本的官方网络安装映像</h2>

<p>该映像包含安装程序及一\
小组可以安装（非常）基本系统\
的软件包，大小至多 300&nbsp;MB。</p>

<div class="line">
<div class="item col50">
<p><strong>网络安装 CD 映像（通过 <a href="$(HOME)/CD/torrent-cd">bittorrent</a>）</strong></p>
	  <stable-netinst-torrent />
</div>
<div class="item col50 lastcol">
<p><strong>网络安装 CD 映像（通常 150-300 MB，因架构而异）</strong></p>
	  <stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>有关这些文件是什么，以及如何使用这些文件，请\
参阅 <a href="../faq/">FAQ</a>。</p>

<p>一旦您已下载映像，请确保查看\
<a href="$(HOME)/releases/stable/installmanual">有关\
安装过程的详细信息</a>。</p>


# 译者注意: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">包含非自由固件的非官方网络最小化镜像</a></h2>

<div id="firmware_nonfree" class="important">
<p>
如果你电脑中的任何硬件需要需要驱动加载<strong>非自由固件</strong>，你可以 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
下载公共固件 tarballs </a>或下载一个包含了<strong>非自由</strong>固件的<strong>非官方的</strong>镜像。如何使用 tarballs 与如何在安装过程中加载
固件的说明可以参阅
<a href="../../releases/stable/amd64/ch06s04">安装指南</a>。
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">包含了<strong>非自由</strong>固件的<strong>非官方的</strong>镜像</a>
</p>
</div>
