#use wml::debian::template title="Como solicitar a criação de uma lista de discussão"
#use wml::debian::translation-check translation="881ca15fb120b2f30d9ab1613754ba77b339037e" maintainer="Thiago Pezzo (Tico)"

<p>O objetivo deste documento é ajudar as pessoas a criarem uma lista
de discussão em <a href="https://lists.debian.org/">lists.debian.org</a>.</p>

<p>Novas listas devem atender estes pré-requisitos básicos:

<ul>
  <li>Propósito básico.
  <br>
  O tópico da nova lista deverá ser apropriado para discussão em uma lista
  permanente em lists.debian.org.
  <br>
  Algumas discussões se encaixam melhor com simples apelidos de email (aliases),
  e aqui as listas de discussão para manutenção de pacotes simples não são
  adequadas .
  É desnecessário dizer que listas inúteis ou com tópicos não relevantes
  (offtopic) não serão criadas.
  </li>

  <li>Audiência interessada.
  <br>
  As requisições para uma nova lista de discussões devem ser baseadas em uma
  necessidade real de uma nova lista, para um fórum de debates separado, ao invés
  de baseadas apenas na vontade do requisitante. Uma audiência mínima também é
  necessária para evitar pedidos de listas de discussão temporárias ou
  momentâneas.</li>
</ul>

<p>Uma vez que esses dois pré-requisitos tenham sido cumpridos, um pedido
  adequado deve ser enviado como um <a href="$(HOME)/Bugs/Reporting">relatório de bug</a>
  <em>wishlist</em> contra o pseudopacote
  <code><a href="https://bugs.debian.org/lists.debian.org">lists.debian.org</a></code></p>

<p>As informações seguintes são necessárias no relatório de bug:</p>

<div class="centerblock">
<dl>

   <dt><strong>Nome</strong></dt>
   <dd>
      <p>Por favor escolha um nome representativo, curto e único.</p>

      <p>Por favor note que cada nome de lista deve ser prefixado com uma palavra
      única, a mais comum sendo <code>debian-</code> para listas relacionadas
      ao Projeto Debian.</p>

      <p>As listas para projetos externos não precisam de um prefixo, já que
      elas serão criadas como <code><var>listname</var>@other.debian.org</code>.</p>

      <p>As palavras são separadas por hífen, "-", assim uma lista sobre "Foo bar"
      em relação ao Debian seria chamada debian-foo-bar.</p>
   </dd>

   <dt><strong>Argumento</strong></dt>
   <dd>
      <p>Uma explicação completa dos motivos pelos quais você deseja que esta
      lista seja criada.</p>

      <p>Os listmasters se reservam o direito de, preliminarmente, chegar a um
      consenso na lista debian-devel e/ou debian-project.
      Se você sabe que sua requisição é questionável, você pode acelerar o
      processo discutindo o assunto antes de preencher o relatório de bug.</p>
   </dd>

   <dt><strong>Descrição curta</strong></dt>
   <dd>
      <p>É a descrição de uma linha, para exibição nos índices de listas, assim
      faça-a pequena e direta.</p>
   </dd>

   <dt><strong>Descrição longa</strong></dt>
   <dd>
      <p>Essa descrição tem em vista as pessoas que estão procurando uma
      determinada lista para inscreverem-se, assim deve ser clara e informativa.</p>

      <p>Veja a <A HREF="subscribe">página de inscrição</A> para exemplos.</p>
   </dd>

   <dt><strong>Categoria</strong></dt>
   <dd>
      <p>Isso é necessário para classificar a lista e colocá-la na categoria correta
      na <a href="subscribe">página de inscrição</a> e em outros lugares.</p>
   <p>
    As categorias disponíveis podem ser encontradas na <a
    href="https://lists.debian.org">página inicial de lists.debian.org</a>.
    </p>
   </dd>

   <dt><strong>Política de inscrições</strong></dt>
   <dd>
      <p>aberta / fechada

      <p>Se for fechada, quem pode se cadastrar, quem pode aprovar
      requisições de subscrição? Nós normalmente não apoiamos listas
      fechadas, você precisará de bons argumentos para requisitar uma lista como
      essa.</p>
   </dd>

   <dt><strong>Política de envio de mensagem</strong></dt>
   <dd>
      <p>aberta / moderada

      <p>Se for moderada, quem serão os(as) moderadores(as)?
         Nós normalmente não apoiamos listas moderadas, você precisará de bons
         argumentos para requisitar uma lista como essa. E também não existe uma
         interface web ou outro modo para isso. A moderação é feita somente
         por e-mail.
      </p>
   </dd>

   <dt><strong>Arquivamento web</strong></dt>
   <dd>
      <p>sim / não

      <p>Nos nossos <a href="https://lists.debian.org/">arquivos de listas de
      discussão</a>. Nós realmente esperamos que os arquivos de listas sejam
      públicos, você precisa de um bom argumento para não ter um arquivamento web.
   </dd>

</dl>
</div>

<p>Depois de preencher a requisição, ficaríamos gratos(as) se outras pessoas com
interesse na nova lista enviassem uma mensagem para o relatório de bug, para que
interesse delas fique registrado.</p>

<p>Por favor, siga as regras acima, uma vez que requisições impróprias não serão
implementadas.</p>

<h3>Movendo listas de discussões existentes para lists.debian.org</h3>

<p>Uma lista de discussões existente pode ser movida para lists.debian.org:
o(a) administrador(a) da lista tem que enviar uma requisição conforme descrito
acima e nos dar uma lista de assinantes (em texto puro, um endereço por linha).
Os arquivos da lista em questão também podem ser importados de formatos de arquivo
mbox (preferencialmente separado por mês).</p>

<p>Além disso, deve-se configurar os redirecionadores da localização antiga
para a nova, claro.</p>
