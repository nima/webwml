<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A few issues were found in Atril, the MATE document viewer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000159">CVE-2017-1000159</a>

    <p>When printing from DVI to PDF, the dvipdfm tool was called without
    properly sanitizing the filename, which could lead to a command
    injection attack via the filename.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>

    <p>The tiff_document_render() and tiff_document_get_thumbnail() did
    not check the status of TIFFReadRGBAImageOriented(), leading to
    uninitialized memory access if that funcion fails.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010006">CVE-2019-1010006</a>

    <p>Some buffer overflow checks were not properly done, leading to
    application crash or possibly arbitrary code execution when
    opening maliciously crafted files.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.8.1+dfsg1-4+deb8u2.</p>

<p>We recommend that you upgrade your atril packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1882.data"
# $Id: $
