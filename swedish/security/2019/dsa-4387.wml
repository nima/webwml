#use wml::debian::translation-check translation="19fdc288616ee3bfe6ee122b16cd10940121ffb2" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Harry Sintonen från F-Secure Corporation upptäckte flera sårbarheter i
OpenSSH, en implementation av SSH-protokollet. Alla sårbarheterna
hittas i scp-klienten som implementerar SCP-protokollet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20685">CVE-2018-20685</a>

	<p>På grund av otillräcklig validering av mappnamn tillåter scp-klienten
	servrar att modifiera rättigheter i målmappen genom att använda tomma
	eller punkt-mappnamn.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6109">CVE-2019-6109</a>

	<p>På grund av saknad teckenkodning i förloppsindikatorn kan objektnamnet
	användas för att manipulera klientutdata, exempelvis för att utnyttja
	ANSI-koder för att gömma ytterligare filer som överförs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6111">CVE-2019-6111</a>

	<p>På grund av otillräcklig validering av indata i scp-klienten i
	sökvägsnamn som skickas av servern kan en illasinnad server göra
	godtyckliga filöverskrivningar i målmappen. Om alternativet kopiera mappar
	rekursivt (-r) används kan servern även manipulera undermappar.</p>
	
	<p>Kontrollen som läggs till i denna version kan leda till regression om
	klienten och servern har olikheter i expansionsregler för jokertecken. Om
	servern är pålitlig för detta ändamål kan kontrollen inaktiveras med ett
	nytt -T-alternativ i scp-klienten.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 1:7.4p1-10+deb9u5.</p>

<p>Vi rekommenderar att ni uppgraderar era openssh-paket.</p>

<p>För detaljerad säkerhetsstatus om openssh vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/openssh">\
https://security-tracker.debian.org/tracker/openssh</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4387.data"
