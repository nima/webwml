<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a HTTP request smuggling
vulnerability in waitress, pure-Python WSGI server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16789">CVE-2019-16789</a>

    <p>In Waitress through version 1.4.0, if a proxy server is used in front of
    waitress, an invalid request may be sent by an attacker that bypasses the
    front-end and is parsed differently by waitress leading to a potential for
    HTTP request smuggling. Specially crafted requests containing special
    whitespace characters in the Transfer-Encoding header would get parsed by
    Waitress as being a chunked request, but a front-end server would use the
    Content-Length instead as the Transfer-Encoding header is considered
    invalid due to containing invalid characters. If a front-end server does
    HTTP pipelining to a backend Waitress server this could lead to HTTP
    request splitting which may lead to potential cache poisoning or unexpected
    information disclosure. This issue is fixed in Waitress 1.4.1 through more
    strict HTTP field validation.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.8.9-2+deb8u1.</p>

<p>We recommend that you upgrade your waitress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2056.data"
# $Id: $
