#use wml::debian::translation-check translation="8a159b4486c5735a9f07d32e1ad7693d155b7886" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BIND, une implémentation
de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25214">CVE-2021-25214</a>

<p>Greg Kuechle a découvert qu'un transfert entrant IXFR mal formé pourrait
déclencher un échec d'assertion dans named, avec pour conséquence un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25215">CVE-2021-25215</a>

<p>Siva Kakarla a découvert que named pourrait planter quand un enregistrement
de DNAME placé dans la section ANSWER durant la poursuite de DNAME se révélait
être la réponse finale à une requête d'un client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25216">CVE-2021-25216</a>

<p>L'implémentation de SPNEGO utilisée par BIND est prédisposée à une
vulnérabilité de dépassement de tampon. Cette mise à jour bascule vers
l'implémentation de SPNEGO provenant des bibliothèques Kerberos.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:9.10.3.dfsg.P4-12.3+deb9u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2647.data"
# $Id: $
