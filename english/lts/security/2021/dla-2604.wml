<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Moshe Kol and Shlomi Oberman of JSOF discovered several
vulnerabilities in dnsmasq, a small caching DNS proxy and DHCP/TFTP
server. They could result in denial of service, cache poisoning or the
execution of arbitrary code.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.76-5+deb9u3.</p>

<p>We recommend that you upgrade your dnsmasq packages.</p>

<p>For the detailed security status of dnsmasq please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/dnsmasq">https://security-tracker.debian.org/tracker/dnsmasq</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2604.data"
# $Id: $
