#use wml::debian::translation-check translation="5fbf8d4a1efd1f97a8e8e756acbe29c729555969" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à l'exécution de code arbitraire, une élévation des
privilèges, un déni de service ou une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12351">CVE-2020-12351</a>

<p>Andy Nguyen a découvert un défaut dans l’implémentation du Bluetooth dans la
manière dont des paquets L2CAP avec CID A2MP sont gérés. Un attaquant distant
peu éloigné connaissant l’adresse du périphérique Bluetooth de la victime peut
envoyer un paquet l2cap malveillant et causer un déni de service ou,
éventuellement, une exécution de code arbitraire avec les privilèges du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12352">CVE-2020-12352</a>

<p>Andy Nguyen a découvert un défaut dans l’implémentation du Bluetooth. La
mémoire de pile n’est pas correctement initialisée lors du traitement de
certains paquets AMP. Un attaquant distant peu éloigné connaissant l’adresse du
périphérique Bluetooth de la victime peut récupérer des informations de la pile
du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25211">CVE-2020-25211</a>

<p>Un défaut a été découvert dans le sous-système netfilter. Un attaquant local
capable d’injecter une configuration de conntrack de Netlink peut provoquer un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25643">CVE-2020-25643</a>

<p>ChenNan de Chaitin Security Research Lab a découvert un défaut dans le
module hdlc_ppp. Une validation incorrecte d’entrée dans la fonction
ppp_cp_parse_cr() pourrait conduire à une corruption de mémoire et une
divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25645">CVE-2020-25645</a>

<p>Un défaut a été découvert dans le pilote d’interface pour le trafic
encapsulé de GENEVE lorsqu’il est combiné avec IPsec. Si IPsec est configuré
pour crypter le trafic pour le port spécifique UDP utilisé par le tunnel de
GENEVE, les données envoyées dans ce tunnel ne sont pas correctement dirigées
à travers la liaison cryptée et sont envoyées non cryptées à la place.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.19.152-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.19.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-4.19, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-4.19">https://security-tracker.debian.org/tracker/linux-4.19</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2417.data"
# $Id: $
