<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A double-free vulnerability was discovered in the gdImagePngPtr()
function in libgd2, a library for programmatic graphics creation and
manipulation, which may result in denial of service or potentially the
execution of arbitrary code if a specially crafted file is processed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.36~rc1~dfsg-6.1+deb7u10.</p>

<p>We recommend that you upgrade your libgd2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1106.data"
# $Id: $
